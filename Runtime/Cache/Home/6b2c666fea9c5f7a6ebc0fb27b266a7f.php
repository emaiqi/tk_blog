<?php if (!defined('THINK_PATH')) exit();?><!--载入头部-->
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo ($cocolait); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/Public/home/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="/Public/home/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="/Public/home/css/index.css"/>
    <!--加载晃动css样式Shake-->
    <link rel="stylesheet" type="text/css" href="/Public/home/css/csshake.min.css"/>
    <!--加载animate动画样式-->
    <link rel="stylesheet" type="text/css" href="/Public/home/css/animate.css"/>
    <!--加载自定义的page样式-->
    <link rel="stylesheet" type="text/css" href="/Public/home/css/Page_tp3.css"/>
    <!--加载分享插件样式-->
    <link rel="stylesheet" href="/Public/plugins/share/css/share.min.css">
    <script src="/Public/home/bootstrap/js/jquery-1.11.3.js" type="text/javascript" charset="utf-8"></script>
    <script src="/Public/home/bootstrap/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/Public/plugins/layer/layer.js" type="text/javascript" charset="utf-8"></script>
    <script src="/Public/home/js/common.js" type="text/javascript" charset="utf-8"></script>
    <!--加载分享插件-->
    <script src="/Public/plugins/share/js/jquery.share.min.js"></script>
    <script type="text/javascript">
        var user_id = "<?php echo ($_SESSION['user']['uid']); ?>";
    </script>
    
    <link rel="shortcut icon" href="/Public/home/images/favicon.ico">
    
</head>
<body>
<!--载入头部导航-->
<!--摸态框 START-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="rl-modal-header" id="rl-modal-header">
                <h1>
                    <span class="active-title">登录</span>
                    <span class="">注册</span>
                </h1>
                <button type="button" class="rl-close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <div class="send_l">
                    <div class="ipt">
                        <div class="form-group mb5">
                            <input type="text" class="form-control" placeholder="请输入登录邮箱" value="" id="cp-lg-email-v">
                            <p class="errorHint" id="cp-lg-email"></p>
                        </div>
                        <div class="form-group mb5">
                            <input type="password" class="form-control" placeholder="请输入登录密码" value="" id="cp-lg-pwd-v">
                            <p class="errorHint" id="cp-lg-pwd"></p>
                        </div>
                    </div>
                    <button type="button" class="btn btn-danger btn-new" id="loginForm">登&nbsp;录</button>
                </div>

                <div class="send_l" style="display: none;">
                    <div class="ipt">
                        <div class="form-group mb5">
                            <input type="text" class="form-control"  placeholder="请输入邮箱" name="email" id="cp-email">
                            <p class="errorHint" id="error-cp-email"></p>
                        </div>
                        <div class="form-group register-wamp mb5">
                            <input type="text" class="form-control register" placeholder="请输入验证码" name="code" id="cp-code">
                            <p class="errorHint" style="width: 60%;" id="error-cp-code"></p>
                            <a href="javascript:;" class="verify-img-wrap Noline">
                                <img src="<?php echo U('Register/verifycode');?>" class="verify-img Noline" title="点击刷新验证码" id="code-img" style="width: 95px;">
                            </a>
                        </div>
                    </div>
                    <button type="button" class="btn btn-danger btn-new Noline" id="registerForm">注&nbsp;册</button>
                </div>

            </div>
            <div class="modal-footer">
                <div class="pop-login-sns">
                    <span style="color:#666;margin-left: 18px;">其他方式登录</span>
                    <a href="<?php echo U('Api/Oauth/login',array('type'=>qq));?>"><i class="icon-qqs"></i></a>
                    <a href="<?php echo U('Api/Oauth/login',array('type'=>sina));?>"><i class="icon-weibos"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--摸态框  END-->





<!--头部 START-->
<header>
    <nav class="common-topWarp navbar-fixed-top">
        <div class="container">
            <!--左侧区域 开始-->
            <div class="top-left">
                <!-- logo start -->
                <div class="mg-logo hidden-xs">
                    <a href="/"></a>
                </div>
                <!-- end logo -->

                <!-- 导航 区域 START-->
                <div class="mg-top-nav">
                    <nav class="collapse navbar-collapse bs- navbar-collapse">
                        <ul class="nav navbar-nav">
                            <?php $cData = M('Category')->where(array('status'=>1,'parent'=>0))->order('`order` ASC')->select();?>
                            <li><a href="/" <?php if(CONTROLLER_NAME == 'Index' && ACTION_NAME == 'index' ): ?>class="active Noline"<?php else: ?>class="Noline"<?php endif; ?> >首页</a></li>
                            <?php if(is_array($cData)): foreach($cData as $key=>$v): ?><li><a href="/sc/<?php echo ($v['cid']); ?>.html" <?php if($v['cid'] == $cid): ?>class="active Noline"<?php else: ?>class="Noline"<?php endif; ?>><?php echo ($v['name']); ?></a></li><?php endforeach; endif; ?>
                        </ul>
                    </nav>
                </div>
                <!-- 导航 区域 END -->
            </div>
            <!--左侧区域 介绍-->


            <!--右侧区域  开始-->
            <div class="top-right hidden-xs hidden-sm">
                <!-- 搜索框区域 START -->
                <div class="mg-search-box hidden-xs hidden-sm">
                    <form class="navbar-search" id="global_search_form" action="/Search" method="get">
                        <?php $k_name = isset($_GET['k'])?$_GET['k']:'';?>
                        <input class="form-conetnt search-query" type="text" placeholder="搜索文章关键字" name="k" style="width: 275px;" value="<?php echo ($k_name); ?>">
                        <i class="icon-search" id="Search-click"></i>
                    </form>
                </div>
                <!-- 搜索框区域 END-->

                <div class="mg-user-nav hidden-xs hidden-sm">
						<span>
                            <?php if(!$_SESSION['user']): ?><a class="login" href="javascript:;" data-toggle="modal" data-target="#myModal">
                                    <i class="icon-login"></i>
                                    <span>登录</span>
                                </a>
                            <?php else: ?>
                                <!--登录后显示的内容 START-->
                                <span class="dropdown" id="msg-show">
                                    <?php if($msg['comment']['total'] && CONTROLLER_NAME != 'Comments'): ?><div>
                                            <i class="icon-msgs animated pulse"></i>
                                            <span class="badge badge2 animated pulse" style="margin-left: 10px;" id="getMSG2"><?php echo ($msg['comment']['total']); ?></span>
                                        </div><?php endif; ?>
                                    <a class="avatar dropdown-toggle" href="javascript:;" data-toggle="dropdown" id="dropdownMenu1">
                                        <span id="avatar-a"><?php echo ($_SESSION['user']['nickname']); ?></span>
                                        <img src="<?php echo ($_SESSION['user']['face']); ?>" alt="<?php echo ($_SESSION['user']['nickname']); ?>">
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="/msg">消息<span class="badge" style="margin-left: 60px;background-color: #f4645f;" id="getMSG"><?php echo ($msg['comment']['total']); ?></span></a></li>
                                        <li><a href="<?php echo U('Api/Oauth/loginOut');?>">退出</a></li>
                                    </ul>
                                </span>
                                <!--登录后显示的内容 END--><?php endif; ?>
						</span>
                </div>
            </div>
            <!--右侧区域  结束-->
        </div>
    </nav>
</header>
<!--头部 END-->
<!--主体内容 START-->
<!--主体内容 START-->
<main>
	<div class="container mt70">
		<div class="row">
			<!--左侧排版 START-->
			<div class="col-md-8" style="padding-right: 0px;">
				<!--文章内容主体 START-->
				<article class="well">
					<!--面包屑 START-->
					<div>
						<ol class="breadcrumb">
							<li><a href="/" class="Noline">首页</a></li>
							<li><a href="/sc/<?php echo ($data['cid']); ?>" class="Noline"><?php echo ($data['cname']); ?></a></li>
							<li class="active"><?php echo ($data['title']); ?></li>
						</ol>
					</div>
					<!--面包屑 END-->
					<div class="content-wamp">
						<div class="title-article">
							<h1><a href="javascript:;"><?php echo ($data['title']); ?></a></h1>
						</div>
						<div class="tag-article">
							<a href="javascript:;">
								<i class="icon-time"></i>
								<span><?php echo (date("n-d",$data['add_time'])); ?></span>
							</a>
							<a href="/st/<?php echo ($data["tid"]); ?>">
								<i class="icon-tag"></i>
								<span><?php echo ($data['tname']); ?></span>
							</a>
							<a href="javascript:;">
								<i class="icon-author"></i>
								<span><?php echo ($data['author']); ?></span>
							</a>
							<a href="javascript:;">
								<i class="icon-browse"></i>
								<span><?php echo ($data['hits']); ?></span>
							</a>
						</div>
						<div class="content-article">
							<!--文章图片 START-->
							<figure class="thumbnail">
								<a href="javascript:;">
									<img src="<?php echo ($data['img']); ?>" class="attachment-full wp-post-image" style="width:750px;height: 350px;">
								</a>
							</figure>
							<!--文章图片 END-->

							<!--描述 开始-->
							<div class="alert-content alert-danger">
								<?php echo ($data['excerpt']); ?>
							</div>
							<!--描述 结束-->

							<!--内容 START-->
							<blockquote>
								<?php echo ($data['content']); ?>
							</blockquote>
							<!--内容 END-->


							<!--分享 START-->
							<div>
								<div id="cp-parents-whs" style="padding-bottom: 6px;"></div>
							</div>
							<script>
								$('#cp-parents-whs').share({sites: ['qq', 'wechat', 'qzone','weibo']});
							</script>
							<!--分享 END-->

							<!--上下篇 START-->
							<div style="border-top: 1px solid #ccc;">
								<ul class="pager">
									<li>
										<a href="javascript:;" class="Noline" id="praise" url="<?php echo U('Article/praise');?>" aid="<?php echo ($data['aid']); ?>" user="<?php echo ($_SESSION['user']['uid']); ?>" style="padding: 5px 9px;">
											<i class="icon-dianzan icon-zan"></i>
											<span> 点赞</span>
											<span class="badge" style="margin-left: 2px;margin-top: -4px;background-color: #f4645f;" id="praise-nums"><?php echo ($data['praise']); ?></span>
										</a>
									</li>
								</ul>
							</div>
							<!--上下篇 END-->

							<!--版权描述 开始-->
							<div class="alert-content alert-success">
								<?php if($prev = getArticlePos($data['aid'],'prev')):?>
								<p class="p-wamp">上一篇&nbsp;:&nbsp;<a href="<?php echo ($prev['url']); ?>" class="Noline"><?php echo ($prev['title']); ?></a></p>
								<?php endif;?>
								<?php if($next = getArticlePos($data['aid'],'next')):?>
								<p class="p-wamp">下一篇&nbsp;:&nbsp;<a href="<?php echo ($next['url']); ?>" class="Noline"><?php echo ($next['title']); ?></a></p>
								<?php endif;?>
								<p class="p-wamp">原文地址&nbsp;:&nbsp;<a href="" class="Noline"><?php echo $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];?></a></p>
							</div>
							<!--版权描述 结束-->

						</div>
					</div>
				</article>
				<!--文章内容主体 END-->

				<!--评论区域 START-->
				<div class="comments-wrap">

    <!--内容展示区域 START-->
    <ol class="commentlist" id="show-content">
        <?php if($Counts): ?><h3 class="comment-reply-title show-h3 alert alert-success" style="padding: 12px;font-size: 18px;margin: 0;text-align: center" id="count-conment" value="<?php echo ($Counts); ?>"><i class="icon-xixi"></i><span style="margin-left: 3px;" id="show-commentCount">共 <?php echo ($Counts); ?> 条评论</span></h3>
            <?php else: ?>
            <h3 class="comment-reply-title show-h3 alert alert-success" style="padding: 10px;font-size: 14px;margin: 0;" id="count-conment" value="0"><i class="icon-xixi"></i><span style="margin-left: 3px;" id="show-commentCount">暂无评论,赶紧发表你的见解吧.^_^ </span></h3><?php endif; ?>

        <?php if(is_array($CommentData)): foreach($CommentData as $key=>$v): ?><li id="comment-<?php echo ($v['id']); ?>" class="comment">
                <article id="div-comment-<?php echo ($v['id']); ?>" class="comment-body">
                    <footer class="comment-meta">
                        <div class="comment-author">
                            <?php if($v['img']): ?><img alt="<?php echo ($v['nickname']); ?>" src="<?php echo ($v['img']); ?>" class="avatar avatar-70 photo" style="width: 70px;height: 70px;">
                                <?php else: ?>
                                <img alt="<?php echo ($v['nickname']); ?>" src="/Public/home/images/face.jpg" class="avatar avatar-70 photo" style="width: 70px;height: 70px;"><?php endif; ?>
                            <b class="fn"><a href="javascript:;" rel="external nofollow" class="url"><?php echo ($v['nickname']); ?></a></b>
                            <span class="says">说道：</span>
                        </div>

                        <div class="comment-metadata">
                            <a href="javascript:;">
                                <time datetime="<?php echo ($v['createtime']); ?>"><?php echo ($v['createtime']); ?></time>
                            </a>
                        </div>

                    </footer>

                    <div class="comment-content">
                        <p><?php echo replace_phiz($v['content']);?></p>
                    </div>

                    <div class="reply">
                        <a class="comment-reply-link Noline" href="javascript:;" toUid=<?php echo ($v['uid']); ?> parentId="<?php echo ($v['id']); ?>" comid="<?php echo ($v['id']); ?>" level="1" postID="<?php echo ($v['id']); ?>">回复</a>
                    </div>
                </article>

                <?php if(v.child): ?><!--第二层 START-->
                    <ol class="children">
                        <?php if(is_array($v["child"])): foreach($v["child"] as $key=>$vv): ?><li id="comment-<?php echo ($vv['id']); ?>" class="comment">
                                <article id="div-comment-<?php echo ($vv['id']); ?>" class="comment-body">
                                    <footer class="comment-meta">
                                        <div class="comment-author">
                                            <?php if($vv['img']): ?><img alt="<?php echo ($vv['nickname']); ?>" src="<?php echo ($vv['img']); ?>" class="avatar avatar-70 photo" style="width: 70px;height: 70px;">
                                                <?php else: ?>
                                                <img alt="<?php echo ($vv['nickname']); ?>" src="/Public/home/images/face.jpg" class="avatar avatar-70 photo" style="width: 70px;height: 70px;"><?php endif; ?>
                                            <b class="fn"><a href="javascript:;" rel="external nofollow" class="url"><?php echo ($vv['nickname']); ?></a></b>
                                            <span class="says">回复<span style="margin-left: 7px;"><?php echo ($vv['toNickname']); ?></span>：</span>
                                        </div>

                                        <div class="comment-metadata">
                                            <a href="javascript:;">
                                                <time datetime="<?php echo ($vv['createtime']); ?>"><?php echo ($vv['createtime']); ?></time>
                                            </a>
                                        </div>

                                    </footer>

                                    <div class="comment-content">
                                        <p><?php echo replace_phiz($vv['content']);?></p>
                                    </div>

                                    <div class="reply">
                                        <a class="comment-reply-link Noline" href="javascript:;" toUid=<?php echo ($vv['uid']); ?> parentId="<?php echo ($vv['parentid']); ?>" comid="<?php echo ($v['id']); ?>" level="2" postID="<?php echo ($vv['id']); ?>">回复</a>
                                    </div>

                                </article>
                            </li><?php endforeach; endif; ?>
                        <span id="show-comment-<?php echo ($v['id']); ?>"></span>
                    </ol>
                    <!--第二层 END--><?php endif; ?>
            </li><?php endforeach; endif; ?>


    </ol>
    <!--内容展示区域 END-->

    <!--分页区域 START-->
    <?php if($page && $pg_total > $pg_limit): ?><div class="pagination" style="margin-bottom: 0px;">
            <div style="float: right">
                <?php echo ($page); ?>
            </div>
        </div><?php endif; ?>
    <!--分页区域 END-->


    <!--发布内容区域 START-->
    <div id="respond" class="comment-respond">
        <h3 id="reply-title" class="comment-reply-title comment-reply-title-cp" style="cursor: pointer" status="1" uid="<?php echo ($_SESSION['user']['uid']); ?>"><i class="icon-edit"></i> 欢迎留言 <small><a rel="nofollow" id="cancel-comment-reply-link" href="" style="display:none;">取消回复</a></small></h3>
        <form id="commentform" class="comment-form" style="display: none">
            <script type="text/javascript">
                var sendCommentUrl = "<?php echo U('Comments/send_comment');?>";
            </script>
            <!--载入表情库 START-->
            <div id="smilelink">
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/han.gif" title="汗" alt="汗"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/se.gif" title="色" alt="色"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/bei.gif" title="悲" alt="悲"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/bizui.gif" title="闭嘴" alt="闭嘴"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/tiaopi.gif" title="调皮" alt="调皮"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/xiao.gif" title="笑" alt="笑"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/jing.gif" title="惊" alt="惊"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/qing.gif" title="亲" alt="亲"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/chan.gif" title="馋" alt="馋"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/yun.gif" title="晕" alt="晕"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/ku.gif" title="酷" alt="酷"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/jian.gif" title="奸" alt="奸"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/nu.gif" title="怒" alt="怒"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/kuang.gif" title="狂" alt="狂"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/meng.gif" title="萌" alt="萌"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/chi.gif" title="吃" alt="吃"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/tan.gif" title="贪" alt="贪"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/jion.gif" title="囧" alt="囧"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/xiu.gif" title="羞" alt="羞"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/kule.gif" title="哭" alt="哭"></a>
    <a href="javascript:;" class="phiz Noline"><img src="/Public/home/biaoqing/he.gif" title="嘿" alt="嘿"></a>
</div>
            <!--载入表情库 START-->
            <textarea id="comment" placeholder="赶快发表你的见解吧！" name="comment" cols="45" rows="8"></textarea>
            <p class="form-submit">
                <input name="submit" type="submit" id="submit" value="发表评论">
                <input type="hidden" name="uid" value="<?php echo ($_SESSION['user']['uid']); ?>" id="uids">
                <input type="hidden" name="parentId" value="0" id="parentId">
                <input type="hidden" name="toUid" value="0" id="toUid">
                <input type="hidden" name="mid" value="0" id="Mid">
                <input type="hidden" name="level" value="0" id="Level">
                <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI']?>">
                <input type="hidden" name="aid" value="<?php echo ($_GET['aid']); ?>">
                <input type="hidden" name="post_id" value="0" id="post-id">
            </p>
        </form>
    </div>
    <!--发布内容区域 END-->
</div>
				<!--评论区域 END-->
			</div>
			<!--左侧排版 END-->

			<!--右侧排版 STRAT-->
			<div class="col-md-4" style="padding-right: 0px; padding-left: 30px;">

    <?php if(CONTROLLER_NAME == 'Index' && ACTION_NAME == 'index' ): ?><aside id="posts-3">
            <div class="widget">
                <h4 class="title">个人专区</h4>
                <div class="content">
                    <div class="account-img">
                        <img alt="Cocolait" src="/Public/home/images/cocolait.jpg" style="width: 100px;height: 100px;">
                    </div>
                    <div class="account-c">
                        <a href="javascript:;" class="account">Cocolait</a>
                        <a href="javascript:;" class="signature">如果还有梦,别只是去想...</a>
                        <a href="http://git.oschina.net/cp.net" class="links" target="_blank">git.oschina.net/cp.net</a>
                    </div>

                </div>

            </div>
        </aside><?php endif; ?>

    <aside id="posts-1">
        <div class="hot-article">
            <div class="panel-heading">
                <span>浏览排行</span>
            </div>
            <ul class="list-group">
                <?php $titleData = M('article')->order("hits DESC")->field('aid,title,hits')->limit(6)->select();?>
                <?php if(is_array($titleData)): foreach($titleData as $key=>$vv): ?><li class="list-group-item">
                      <span class="post-title">
                        <a href="/a/<?php echo ($vv['aid']); ?>.html" title="<?php echo ($vv['title']); ?>"><?php echo msubstr($vv['title'],0,25);?></a>
                      </span>
                      <span class="badge"><?php echo ($vv['hits']); ?></span>
                    </li><?php endforeach; endif; ?>
            </ul>
        </div>
    </aside>

    <aside id="posts-2">
        <div class="widget">
            <h4 class="title">标签云</h4>
            <div class="tag-cloud">
                <?php $tagsData = M('tags')->select();?>
                <?php if(is_array($tagsData)): foreach($tagsData as $key=>$t): ?><a href="/st/<?php echo ($t['tid']); ?>.html"><?php echo ($t["tname"]); ?></a><?php endforeach; endif; ?>
            </div>

        </div>

    </aside>

</div>
			<!--右侧排版 END-->

		</div>
	</div>
</main>
<!--主体内容 END-->
<!--载入底部-->
<!--底部 STARET-->
<footer class="footer mt30">
    <div class="w100 navbar-default">
        <div class="container">
            <p class="pull-left"><?php echo C('MG_COPYRIGHT');?> | <?php echo C('MG_ICP');?></p>
            <p class="pull-right"><script src="//push.zhanzhang.baidu.com/push.js"></script><script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1254533699'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s11.cnzz.com/z_stat.php%3Fid%3D1254533699%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));</script><span id="cnzz_stat_icon_1254533699"><a href="http://www.cnzz.com/stat/website.php?web_id=1254533699" target="_blank" title="站长统计"><img border="0" hspace="0" vspace="0" src="http://icon.cnzz.com/img/pic.gif"></a></span><script src=" http://s11.cnzz.com/z_stat.php?id=1254533699&amp;show=pic" type="text/javascript"></script><script src="http://c.cnzz.com/core.php?web_id=1254533699&amp;show=pic&amp;t=z" charset="utf-8" type="text/javascript"></script></p>
        </div>
    </div>
</footer>
<!--底部 END-->

<!--右侧回到顶部 START-->
<a href="javascript:;" id="back-to-top" style="display: none;"><i class="icon-up"></i></a>
<!--右侧回到顶部 END-->
</body>
</html>